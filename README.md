# KomputerStore

Assignment 4 Komputer Store
 
## Table of Contents
- [Features](#features)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Features
Computer store where the user can live out his or her ultimate fantasy life. Working is as simple as clicking a button and the money earned can be used to buy computers. Loans are also handed out like candy, much like in real life. Unleash the power of the pyramid!

## Usage
- Run the site from Visual Studio Code through live server.
- Visit the site on Gitlab pages.

## Gitlab Page Link
[See the page here](https://maaby200.gitlab.io/-/komputerstore/-/jobs/1586216666/artifacts/public/Index.html)

## Front end developer age 20

<img src="./public/src/images/errorimg.png" alt="banner" width="350"/>

## Maintainers
- Alexander Maaby
