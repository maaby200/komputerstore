//Stored values
let bankBalance = 0;
let untransferedSalary = 0;
let currentLoan = 0;
let currentSelectedLaptop = 0;
let hasBoughtComputer = false;
let hasFirstLoan = false;

//Html Text elements
const currentSalaryText = document.getElementById("text-salary");
const currentBalanceText = document.getElementById("text-balance");
const computerHeaderText = document.getElementById("computer-header");
const storeComputerDescriptionText =
  document.getElementById("laptop-description");
const storeComputerPriceText = document.getElementById("laptop-price");
const loanRemainingText = document.getElementById("text-loan-balance");
const laptopFeatureList = document.getElementById("laptop-features-list");

//Laptop select
const laptopSelect = document.getElementById("laptop-select");
const computerImage = document.getElementById("computer-image");

//Assigning buttons to variables
const getLoanButton = document.getElementById("button-loan");
const workButton = document.getElementById("button-work");
const transferSalaryButton = document.getElementById("button-transfer-salary");
const buyLaptopButton = document.getElementById("laptop-button");
const repayLoanButton = document.getElementById("repay-loan");

//Fetch API computer related
let computers = [];
const apiurl = "https://noroff-komputer-store-api.herokuapp.com/";
fetch(apiurl + "computers")
  .then((response) => (computers = response.json()))
  .then((data) => (computers = data))
  .then((computers) => setComputersMenu(computers));

const setComputersMenu = (comps) => {
  comps.forEach((x) => setComputerMenu(x));
};
const setComputerMenu = (comps) => {
  const computerElement = document.createElement("option");
  computerElement.value = comps.id;
  computerElement.appendChild(document.createTextNode(comps.title));
  laptopSelect.appendChild(computerElement);
  changeSelectedLaptop(0);
};
//Refreshes all the data surrounding selected computer, based upon id parameter.
function changeSelectedLaptop(id) {
  computerHeaderText.innerText = computers[id].title;
  storeComputerDescriptionText.innerText = computers[id].description;
  computerImage.src = apiurl + computers[id].image;
  storeComputerPriceText.innerText = computers[id].price + " NOK";
  laptopFeatureList.innerText = computers[id].specs;
}
laptopSelect.addEventListener("change", (event) => {
  changeSelectedLaptop(event.target.value - 1);
  currentSelectedLaptop = event.target.value - 1;
});

//Bank related methods
const applyForLoan = () => {
  if (currentLoan > 0 && bankBalance > 0) {
    alert(
      "Sorry, but the government has decided that you ain't worthy of more than one loan!"
    );
  } else if (!hasBoughtComputer && hasFirstLoan) {
    alert(
      "You can not get more than one loan before buying a computer, where did the money go?!?! Luksusfellen neste (this is your word of the day Dewald)"
    );
  } else {
    let tempLoanHolder = parseInt(
      prompt(
        "How much would you like to loan? (Max is " + bankBalance * 2 + ")"
      )
    );
    while (
      isNaN(tempLoanHolder) ||
      tempLoanHolder < 0 ||
      tempLoanHolder > bankBalance * 2
    ) {
      tempLoanHolder = parseInt(prompt("Please enter a valid amount to loan!"));
    }
    currentLoan += tempLoanHolder;
    bankBalance += tempLoanHolder;
    hasFirstLoan = true;
    updateAllText();
  }
};
getLoanButton.addEventListener("click", () => {
  applyForLoan();
});

//Work related methods
const work = () => {
  untransferedSalary += 100;
  updateAllText();
};
const transferSalaryToBank = () => {
  //Handle that 10% of salary has to go towards loan, if one is present.
  if (currentLoan > 0) {
    //If remaining loan is higher than 10% of salary to be transfered
    if (untransferedSalary * 0.1 < currentLoan) {
      const loanDeduction = untransferedSalary * 0.1;
      currentLoan -= loanDeduction;
      untransferedSalary -= loanDeduction;
    }
    //Else salary will completely cover the remainder of the loan
    else {
      untransferedSalary -= currentLoan;
      currentLoan = 0;
    }
    bankBalance += untransferedSalary;
    untransferedSalary = 0;
  } else {
    bankBalance += untransferedSalary;
    untransferedSalary = 0;
  }
  updateAllText();
};

const repayLoanDirectly = () => {
  if (currentLoan > untransferedSalary) {
    currentLoan -= untransferedSalary;
    untransferedSalary = 0;
  } else {
    untransferedSalary -= currentLoan;
    currentLoan = 0;
  }
  updateAllText();
};
const buyCurrentComputer = () => {
  let currentPrice = parseInt(computers[currentSelectedLaptop].price);
  if (bankBalance >= currentPrice) {
    alert(
      "You bought a " + computers[currentSelectedLaptop].title + " you stud!"
    );
    bankBalance -= currentPrice;
    hasBoughtComputer = true;
    updateAllText();
  } else {
    alert("You gotta work harder, you look the funds for this!");
  }
};
//Updates all the text on the websites based upon the different values.
const updateAllText = () => {
  currentSalaryText.innerText = untransferedSalary + " NOK";
  currentBalanceText.innerText = "Balance     " + bankBalance + " NOK";
  loanRemainingText.innerText = "Loan     " + currentLoan + " NOK";
  if (currentLoan > 0) {
    loanRemainingText.style.display = "block";
    repayLoanButton.style.display = "block";
  } else {
    loanRemainingText.style.display = "none";
    repayLoanButton.style.display = "none";
  }
};
//Button listeners
//Adds a listener to the work button and calls work()
workButton.addEventListener("click", () => {
  work();
});
//Adds a listener to the transfer salary button that calls for the transfer
transferSalaryButton.addEventListener("click", () => {
  transferSalaryToBank();
});
repayLoanButton.addEventListener("click", () => {
  repayLoanDirectly();
});
buyLaptopButton.addEventListener("click", () => {
  buyCurrentComputer();
});

computerImage.addEventListener("error", function (event) {
  event.target.src = "./src/images/errorimg.png";
  event.onerror = null;
});
//Update the text as soon as page loads
updateAllText();
